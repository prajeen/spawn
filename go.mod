module gitlab.com/prajeen/spawn

go 1.20

require (
	github.com/carck/libheif v1.12.0
	github.com/kolesa-team/go-webp v1.0.4
	github.com/schollz/progressbar/v3 v3.13.1
	github.com/urfave/cli/v2 v2.25.7
)

require github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.3 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/rivo/uniseg v0.4.4 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20201216005158-039620a65673 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/term v0.13.0 // indirect
)
