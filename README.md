<!--
SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>

SPDX-License-Identifier: MPL-2.0
-->

# Spawn - A cli tool to generate dummy files

`spawn` is a cli which is build to generate various types of dummy files which
are mostly needed for testing when developing a file-based software. Currently,
the cli tool is mostly focused on working with media files and to generate 
them.

### Dependencies
- Requires [libwebp](https://developers.google.com/speed/webp/docs/api) for webp image generation.
- Requires [libheif](https://github.com/strukturag/libheif) for heif image generation.

### License
This work is licensed under multiple licences. Because keeping this section up-to-date is challenging, here is a brief summary as of October 2023:

- All original source code is licensed under MPL-2.0.
- Some configuration and data files are licensed under CC0-1.0.

For more accurate information, check the individual files.
