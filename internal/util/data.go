// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package util

import (
	"crypto/rand"
	"log"
)

func GetRandomBytes(size uint) (n int, b []byte) {
	b = make([]byte, size)
	n, err := rand.Read(b)
	if err != nil {
		log.Fatalf("Cryptographic random data generation failed: %v", err)
	}

	return n, b
}
