// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package util

import (
	"log"
	"sync"

	pb "github.com/schollz/progressbar/v3"
)

type SyncProgress struct {
	m   sync.Mutex
	bar pb.ProgressBar
}

func NewSyncProgress(count uint, desc string) *SyncProgress {
	return &SyncProgress{
		bar: *pb.Default(int64(count), desc),
	}
}

func (sp *SyncProgress) Increment() {
	sp.m.Lock()
	err := sp.bar.Add(1)
	sp.m.Unlock()
	if err != nil {
		log.Printf("Progress bar update failed: %v", err)
	}
}
