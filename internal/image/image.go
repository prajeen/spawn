// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package image

import (
	"fmt"
	"image"
	"image/color"
	"image/draw"
	"runtime"
	"sync"

	"gitlab.com/prajeen/spawn/internal/util"
)

type Dimens struct {
	Width  uint
	Height uint
}

type Generator interface {
	Generate(string, *Dimens)
}

func BatchProcess(g Generator, dir, prefix string, d *Dimens, count uint) {
	util.CreateFolderIfNotExits(dir)
	sbar := util.NewSyncProgress(count, "Batch generating images")
	batch := make(chan struct{}, runtime.NumCPU())
	var wg sync.WaitGroup
	wg.Add(int(count))
	for i := uint(1); i <= count; i++ {
		filename := fmt.Sprintf("%s/%s_%dx%d_%d", dir, prefix, d.Width, d.Height, i)
		batch <- struct{}{}
		go func() {
			defer wg.Done()
			g.Generate(filename, d)
			sbar.Increment()
			<-batch
		}()
	}
	wg.Wait()
}

func convertNrgbaToRgba(im image.Image) image.Image {
	b := im.Bounds()
	rgba := image.NewRGBA(image.Rect(0, 0, b.Dx(), b.Dy()))
	draw.Draw(rgba, rgba.Bounds(), im, b.Min, draw.Src)
	return rgba
}

func generateImage(d *Dimens) image.Image {
	img := image.NewNRGBA(image.Rect(0, 0, int(d.Width), int(d.Height)))
	n, b := util.GetRandomBytes(d.Width * d.Height * 3)

	for x := 0; x < int(d.Width); x++ {
		for y := 0; y < int(d.Height); y++ {
			img.Set(x, y, color.NRGBA{
				R: b[n-1],
				G: b[n-2],
				B: b[n-3],
				A: 100,
			})
			n -= 3
		}
	}

	return img
}
