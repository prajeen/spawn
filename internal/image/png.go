// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package image

import (
	"fmt"
	"image/png"
	"log"

	"gitlab.com/prajeen/spawn/internal/util"
)

type PngGenerator struct{}

// interface guards
var _ Generator = (*PngGenerator)(nil)

func (p *PngGenerator) Generate(name string, d *Dimens) {
	if !util.CheckExtension(name, "png") {
		name = fmt.Sprintf("%s.png", name)
	}

	file := util.CreateFile(name)
	defer file.Close()

	image := generateImage(d)

	if err := png.Encode(file, image); err != nil {
		log.Printf("Cannot create png file from image: %v", err)
	}
}
