// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package image

import (
	"fmt"
	"image/jpeg"
	"log"

	"gitlab.com/prajeen/spawn/internal/util"
)

type JpegGenerator struct {
	Quality int
}

// interface guards
var _ Generator = (*JpegGenerator)(nil)

func (j *JpegGenerator) Generate(name string, d *Dimens) {
	if !util.CheckExtension(name, "jpg") || !util.CheckExtension(name, "jpeg") {
		name = fmt.Sprintf("%s.jpg", name)
	}

	file := util.CreateFile(name)
	defer file.Close()

	image := generateImage(d)

	err := jpeg.Encode(file, image, &jpeg.Options{
		Quality: j.Quality,
	})
	if err != nil {
		log.Printf("Cannot create jpg file from image: %v", err)
	}
}
