// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package image

import (
	"fmt"
	"log"

	"github.com/kolesa-team/go-webp/encoder"
	"github.com/kolesa-team/go-webp/webp"
	"gitlab.com/prajeen/spawn/internal/util"
)

type WebpGenerator struct {
	Quality float32
}

// interface guards
var _ Generator = (*WebpGenerator)(nil)

func (w *WebpGenerator) generateOptions() *encoder.Options {
	opts, err := encoder.NewLossyEncoderOptions(encoder.PresetDefault, w.Quality)
	if err != nil {
		log.Fatalln("WebP encoder options failed")
	}
	return opts
}

func (w *WebpGenerator) Generate(name string, d *Dimens) {
	if !util.CheckExtension(name, "webp") {
		name = fmt.Sprintf("%s.webp", name)
	}

	file := util.CreateFile(name)
	defer file.Close()

	image := generateImage(d)
	opts := w.generateOptions()

	if err := webp.Encode(file, image, opts); err != nil {
		log.Printf("Cannot create webp file from image: %v", err)
	}
}
