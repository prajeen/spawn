// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package image

import (
	"fmt"
	"log"

	"github.com/carck/libheif/go/heif"
	"gitlab.com/prajeen/spawn/internal/util"
)

type HeifGenerator struct {
	Quality int
}

// interface guards
var _ Generator = (*HeifGenerator)(nil)

func (h *HeifGenerator) Generate(name string, d *Dimens) {
	nrgba := generateImage(d)
	image := convertNrgbaToRgba(nrgba)

	ctx, err := heif.EncodeFromImage(image, heif.CompressionHEVC, h.Quality, heif.LosslessModeEnabled, heif.LoggingLevelNone)
	if err != nil {
		log.Fatalln("Cannot create heif file from image")
	}

	if !util.CheckExtension(name, "heic") || !util.CheckExtension(name, "heif") {
		name = fmt.Sprintf("%s.heic", name)
	}

	if err := ctx.WriteToFile(name); err != nil {
		log.Printf("Failed to write heif image: %v", err)
	}
}
