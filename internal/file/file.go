// SPDX-FileCopyrightText: 2023 Prajeen Govardhanam <prajeenrg@gmail.com>
//
// SPDX-License-Identifier: MPL-2.0

package file

import (
	"log"

	"gitlab.com/prajeen/spawn/internal/util"
)

func MakeDummyFile(name string, size uint) {
	file := util.CreateFile(name)
	defer file.Close()

	_, b := util.GetRandomBytes(size)
	_, err := file.Write(b)
	if err != nil {
		log.Printf("Writing to %s failed: %v", name, err)
	}
}
